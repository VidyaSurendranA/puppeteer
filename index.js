const CREDS = require('./creds');
const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            `--start-maximized`
        ],
    });

    const page = await browser.newPage();
    page.setViewport({
        width: 1360,
        height: 729
    });

    await page.goto('https://int-nextconnect.sial.com ', { waitUntil: "domcontentloaded" });
    await page.waitFor(3000);

    //SIGN IN
    const USERNAME = '[name=username]';
    const PASSWORD = '[name=password]';
    const SIGN_IN = 'button';

    await page.type(USERNAME, CREDS.username);
    await page.type(PASSWORD, CREDS.password);

    await page.click(SIGN_IN);
    await page.waitForNavigation();
    await page.waitFor(4000)

    // ACCEPT TERMS AND CONDITIONS
    const I_ACCEPT = '.btn-outline-danger'
    await page.click(I_ACCEPT)
    await page.waitFor(2000)

    //LOAD DEVICES
    const DEVICES = 'div.SideBar__sidebar-nextconnect___2_Ji- a[href="/devices"]'
    await page.click(DEVICES)
    await page.waitFor(3000)

    // Grid view of devices
    await page.click('.Devices__card-body___1tgdC div:nth-child(1) .row div:nth-child(2) span:nth-child(2) g')
    await page.waitFor(2000)

    // Expand Device Type in filters
    await page.click('div:nth-child(2) span.GenericAccordion__filter-expand___1zI9E')
    await page.waitFor(2000)

    //Device details
    await page.click('a[href="/devices/17310/details"]')
    await page.waitFor(4000)

    //Device History
    await page.click('div.Content__inner-wrap___khYS2 div.col-lg-4 button:nth-child(1)')
    await page.waitFor(5000)
    page.click('.DeviceHistory__modal-footer-container___bIlht > button')
    await page.waitFor(4000)

    // Add cards
    await page.click('div.Content__inner-wrap___khYS2 div.col-lg-4 button:nth-child(2)')
    await page.waitFor(2000)
    await page.click('.Desktop__dark-shade___1lNif > .btn-primary')
    await page.waitFor(2000)
    await page.click('.Desktop__dark-shade___1lNif > .btn-muted')
    await page.waitFor(4000)

    //Assessment Card
    await page.click('div.Content__inner-wrap___khYS2 div.col-lg-4 button:nth-child(3)')
    await page.waitFor(3000)

    await page.click('div.Desktop__button-bar___YslOj.Desktop__dark-shade___1lNif > button')
    await page.waitFor(4000)

    //Remote Connection
    await page.click('div.Content__inner-wrap___khYS2 div.col-lg-4 button:nth-child(4)')
    await page.waitFor(3000)

    await page.click('button.btn-muted')
    await page.waitFor(3000)

    //File management
    await page.click('span.GenericAccordion__defaultIconStyle___3I_lU')
    await page.waitFor(3000)

    //Technical Document
    await page.click('span.DocumentTypeFilesListing__fileManagementChildExpandIcon___3LnXz')
    await page.waitFor(3000)

    //CARDS
    const CARDS = 'div.SideBar__sidebar-nextconnect___2_Ji- a[href="/cards"]'
    await page.click(CARDS)
    await page.waitFor(4000)

    //Create Cards
    await page.click('.Cards__create-card-btn-wrapper___27S0C > button')
    await page.waitFor(4000)

    await page.click('.CardsCreation__form-wrapper___3DJCs button.btn-primary')
    await page.waitFor(4000)

    await page.click('.CardsCreation__form-wrapper___3DJCs button.btn-muted')
    await page.waitFor(4000)

    //DEVICE SETTINGS
    const DEVICE_SETTINGS = 'div.SideBar__sidebar-nextconnect___2_Ji- a[href="/devices/registry"]'
    await page.click(DEVICE_SETTINGS)
    await page.waitFor(4000)

    await page.click('.DeviceRegistry__device-table-filter___1ftVN > button:nth-child(1)')
    await page.waitFor(4000)

    await page.click('div.AddDevice__add-device-form-body___10Lv9 button.btn-primary')
    await page.waitFor(4000)

    await page.click('div.AddDevice__add-device-form-body___10Lv9 button.btn-muted')
    await page.waitFor(4000)

    //Device Profile
    await page.click('div.Table__table-container___11dOR.devicesTable a')
    await page.waitFor(4000)


    await page.click('div.Content__inner-wrap___khYS2 fieldset div:nth-child(2) > svg > g > g > path')
    await page.waitFor(4000)

    await page.click('div.AddDeviceLocation__add-device-location-form-body___1rOgU > div > form > fieldset > button')
    await page.waitFor(4000)

    await page.click('div.AddDeviceLocation__add-device-location-form-body___1rOgU > button')
    await page.waitFor(4000)

    await page.click('div.Content__inner-wrap___khYS2 fieldset div:nth-child(4) > svg > g > g > path')
    await page.waitFor(4000)

    await page.click('div.AddDeviceGroup__add-device-group-form-body___3II0k > div > form > fieldset > button')
    await page.waitFor(4000)
    await page.click('div.AddDeviceGroup__add-device-group-form-body___3II0k > div > form > fieldset > div:nth-child(3) > svg')
    await page.waitFor(4000)
    await page.click('div.AddDeviceGroup__add-device-group-form-body___3II0k > button')
    await page.waitFor(4000)


    //DEVICE CERTIFICATE STATUS DASHBOARD
    const DEV_CERTIFICATE = 'div.SideBar__sidebar-nextconnect___2_Ji- a[href="/certificatesstatus"]'
    await page.click(DEV_CERTIFICATE)
    await page.waitFor(4000)

    await page.click('.more-button > a')
    await page.waitFor(4000)

    await page.click('.DeviceCertificateViewMore__modal-footer-container___37YvE > button')
    await page.waitFor(4000)

    //USER MANAGEMENT
    const USER_MANAGEMENT = 'div.SideBar__sidebar-nextconnect___2_Ji- a[href="/users"]'
    await page.click(USER_MANAGEMENT)
    await page.waitFor(4000)


    //Add user
    await page.waitFor(3000)
    await page.click('.Users__user-filter___2hvMW > button:nth-child(1)')
    //invite user 
    await page.waitFor(3000)
    await page.click('div.AddUser__add-user-form-body___22EtM button.btn-primary')
    //invite user
    await page.waitFor(3000)
    await page.click('div.AddUser__add-user-form-body___22EtM button.btn-muted')

    //manage Role
    await page.waitFor(3000)
    await page.click('.Users__user-filter___2hvMW > button.btn-outline-warning')
    await page.waitFor(3000)
    await page.click('div.AddRole__roleAddDeleteRow___2_Anz div.overlay-trigger button')
    await page.waitFor(3000)
    await page.click('div.AddRole__roleAddDeleteRow___2_Anz div.overlay > span > div > div:nth-child(1)')
    await page.waitFor(3000)
    await page.click('div.TabBar__tab-bar___2eJP0 > div:nth-child(2)')
    await page.waitFor(3000)
    await page.click('div.TabBar__tab-bar___2eJP0 > div:nth-child(3)')
    //Back to users 
    await page.waitFor(3000)
    await page.click('div.NavBar__mainbar-nextconnect___jwa8h span.ant-breadcrumb-link > a')

    //manage Organization 
    await page.waitFor(3000)
    await page.click('.Users__user-filter___2hvMW > button:nth-child(3)')
    await page.waitFor(3000)
    await page.click('div.row.GenericListing__user-content___1_Utg div > svg')
    await page.waitFor(3000)
    await page.click('div.ManageOrganization__form-wrap___1aIzC button.btn-primary')
    await page.waitFor(3000)
    await page.click('div.ManageOrganization__form-wrap___1aIzC button.btn-muted')
    await page.waitFor(3000)
    await page.click('div.GenericListing__user-filter___1YD3i > div.Options__options-container___rNNk3 > button')
    await page.waitFor(3000)
    await page.click('div.ManageOrganization__form-wrap___1aIzC button.btn-primary')
    await page.waitFor(3000)
    await page.click('div.ManageOrganization__form-wrap___1aIzC button.btn-muted')
    await page.waitFor(4000)

    await page.click('div.SideBar__sidebar-nextconnect___2_Ji- a[href="/users"]')
    await page.waitFor(3000)
    await page.click('span.SortableTableHeader__sort-icons-wrapper___2cwZ_ > span:nth-child(2)')
    await page.waitFor(3000)
    await page.click('div.Table__table-container___11dOR div:nth-child(5) > div > div > a')
    await page.waitFor(4000)

    //FILE MANAGEMENT
    const FILE_MANAGEMENT = 'div.SideBar__sidebar-nextconnect___2_Ji- a[href="/files"]'
    await page.click(FILE_MANAGEMENT)
    await page.waitFor(4000)


    await page.click('div.Table__table-container___11dOR > div:nth-child(2) > div:nth-child(2) > div > div:nth-child(6) > div > svg')
    await page.waitFor(3000)
    await page.click('div.ViewMoreDetailsFile__modal-footer-container___2yeDm > button')
    await page.waitFor(4000)

    await page.click('div.GenericListing__user-filter___1YD3i > button')
    await page.waitFor(4000)
    await page.click('div.UploadFile__upload-file-form-btns___1J9HE > button.btn-primary')
    await page.waitFor(3000)
    await page.click('div.UploadFile__upload-file-form-btns___1J9HE > button.btn-muted')
    await page.waitFor(4000)

    //GLOBAL SYSTEM ALARMS AND ALERTS
    const ALARMS_ALLERTS = 'div.SideBar__sidebar-nextconnect___2_Ji- a[href="/globalalerts"]'
    await page.click(ALARMS_ALLERTS)
    await page.waitFor(4000)

    //LOGOUT
    const LOG_OUT = 'div.SideBar__sidebar-nextconnect___2_Ji- div.SideBar__sidebar-icon-box___11X5Z a[href="/logout"]'
    await page.click(LOG_OUT)
    browser.close()

})()
